# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Contributor: Helmut Stult
# Contributor: Nate Simon <njsimon10@gmail.com>

pkgname=xviewer
pkgver=3.4.8
pkgrel=1
pkgdesc="Fast and functional graphics viewer"
arch=('x86_64')
url="https://github.com/linuxmint/xviewer"
license=('GPL-2.0-or-later')
groups=('x-apps')
depends=(
  'cinnamon-desktop'
  'exempi'
  'gtk3'
  'libexif'
  'libjpeg-turbo'
  'libpeas'
  'xapp'
)
makedepends=(
  'glib2-devel'
  'gobject-introspection'
  'gtk-doc'
  'itstool'
  'librsvg'
  'meson'
)
checkdepends=(
  'appstream'
)
optdepends=(
  'xviewer-plugins: Extra plugins'
  'librsvg: for scaling svg images'
  'webp-pixbuf-loader: webp support'
  'libheif: HEIF support'
  'libavif: AVIF support'
  'yelp: for viewing the help manual'
)
source=("${pkgname}-${pkgver}.tar.gz::${url}/archive/${pkgver}.tar.gz")
sha256sums=('c8abe0e9a19ba867c620a1c417bb719b6c1e07e5baa444697d1e71ad8f676889')

build() {
  arch-meson "${pkgname}-${pkgver}" build
  meson compile -C build
}

check() {
  desktop-file-validate "build/data/${pkgname}.desktop"
  appstreamcli validate --no-net "build/data/${pkgname}.appdata.xml" || :
}

package(){
  meson install -C build --destdir "${pkgdir}"
}
